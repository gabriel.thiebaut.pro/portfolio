<?php

    /*
    French translation by Gabriel Thiebaut
    August 2021
    */

    /* MENU ITEMS
    ###########################*/
    $item1=         'A propos';
    $item2=         'Compétences';
    $item3=         'Parcours';
    $item4=         'Projets';
    $item5=         'Contact';

    /* HEADER ITEMS
    ######################################################*/
    $firstname=     'Gabriel';
    $lastname=      'Thiebaut';
    $subtitle=      'Développeur Web &  Web Mobile !';

    $aboutme=       "Je m'apelle Gabriel Thiebaut, je suis actuellement en formation à Campus26 afin de devenir Développeur Web et Web Mobile. Je suis passionné par le développement et j'aime beaucoup travailler en équipe. Aujourd’hui, le métier de développeur est un métier d’avenir. Notre monde contemporain est très orienté média et technologie. De nos jours, internet fait partie de notre quotidien et devient de plus en plus poussé et enrichi avec toutes ses technologies. Dans l’avenir, je souhaiterais avoir la chance d’être embauché dans une équipe de développement et de participer à beaucoup de projets passionnants. J’ai beaucoup de centres d'intérêt que pourrait avoir un “Geek”. J’adore les jeux vidéos, l’informatique, la technologie en général.";

    /* SKILLS ITEMS
    #################################*/
    $level=         'Niveau';
    $advanced=      'Avancé';
    $medium=        'Intermédiaire';
    $low=           'Débutant';

    /* CAREER ITEMS
    #######################################################################################*/
    $route1=        'Stage découverte du métier Chargé de communication (Le Puy-en-Velay)';
    $route2=        'Stage découverte du métier Infographiste (Le Puy-en-Velay)';
    $route3=        'Apprentissage de serveur au Majestic (Le Puy-en-Velay)';
    $route4=        'Formation Initiatique développeur web à Campus26 (Le Puy-en-Velay)';
    $route5=        'Formation développeur Web & Web Mobile à Campus26 (Le Puy-en-Velay)';

    /* PROJECTS ITEMS
    ##########################################*/
    $htmlcss=       'HTML / CSS';
    $analyse=       'Analyse & Reproduction';
    $animation=     'Animation JS';
    $bookmarks=     'Bookmarks';
    $portfolio=     'Portfolio';

    /* CONTACT ITEMS
    ###########################*/
    $confirst=      'Prénom';
    $conlast=       'Nom';
    $conmail=       'Email';
    $conmes=        'Message';
    $consub=        'Envoyer';