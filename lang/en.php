<?php 

    /*
    English translation by Gabriel Thiebaut
    August 2021
    */

    /* MENU ITEMS
    ###########################*/
    $item1=         'About';
    $item2=         'Skills';
    $item3=         'Career';
    $item4=         'Projects';
    $item5=         'Contact';


    /* HEADER ITEMS
    ######################################################*/
    $firstname=     'Gabriel';
    $lastname=      'Thiebaut';
    $subtitle=      'French Web & Mobile Web Developer';

    $aboutme=       "My name is Gabriel Thiebaut. I'm now learning at Campus26 to become Web & Web Mobile developer. I'm fond of the developement and I really like to work in a team. Today, the developer job is a future proof job in our connected world. I would like to be hired in a developer team to take part in very interesting projects. I'm a geek, I'm keen on video games, computing and technology in general.";


    /* SKILLS ITEMS
    ###########################*/
    $level=         'Level';
    $advanced=      'Advanced';
    $medium=        'Medium';
    $low=           'Starter';

    /* CAREER ITEMS
    ####################################################################################################*/
    $route1=        'Internship to discover the profession of communication manager (Le Puy-en-Velay)';
    $route2=        'Internship to discover the profession of graphic designer (Le Puy-en-Velay)';
    $route3=        'Waiter apprenticeship in Le Majestic (Le Puy-en-Velay)';
    $route4=        'Initiation training to web developement at Campus26 (Le Puy-en-Velay)';
    $route5=        'Training to Web & Mobile Web developement at Campus26 (Le Puy-en-Velay)';

    /* PROJECTS ITEMS
    ##########################################*/
    $htmlcss=       'HTML / CSS';
    $analyse=       'Analysis & Duplication';
    $animation=     'JS Animation';
    $bookmarks=     'Bookmarks';
    $portfolio=     'Portfolio';

    /* CONTACT ITEMS
    ###############################*/
    $confirst=      'First Name';
    $conlast=       'Last Name';
    $conmail=       'Email';
    $conmes=        'Message';
    $consub=        'Send';