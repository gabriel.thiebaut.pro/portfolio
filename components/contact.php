<div data-aos="fade-in"  id="contact" class="contact">
    <div class="contact-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2819.191001917409!2d3.877423615815971!3d45.041345769579074!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f5fa56cc290811%3A0x7275623c3e21488d!2sCampus26%20-%20La%20formation%20partout%20pour%20tous!5e0!3m2!1sfr!2sfr!4v1629189821654!5m2!1sfr!2sfr" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
    <div class="contact-header">
        <h2 id="important"><?= $item5 ?></h2>
    </div>

    <div class="form">
        <div class="infos">
            <div class="infos-para">
                <p><i class="fas fa-phone"></i> 07 67 36 89 28</p>
                <p><i class="fas fa-envelope"></i> gabriel.thiebaut.pro@gmail.com</p>
                <p><i class="fas fa-city"></i> Solignac-sur-Loire</p>
            </div>
        </div>
        <div class="divform">
            <form action="post">
                <div class="label_div">
                    <label for="firstname">
                        <p><?= $confirst ?></p>
                    </label>
                    <input type="text">
                </div>
                <div class="label_div">
                    <label for="lastname">
                        <p><?= $conlast ?></p>
                    </label>
                    <input type="text">
                </div>
                <div class="label_div">
                    <label for="email">
                        <p><?= $conmail ?></p>
                    </label>
                    <input type="email">
                </div>
                <div class="label_div">
                    <label for="message">
                        <p><?= $conmes ?></p>
                    </label>
                    <textarea></textarea>
                </div>
                <div class="label_div-sub">
                    <input type="submit" value="<?= $consub ?>">
                </div>
            </form>
        </div>
    </div>
</div>