<div data-aos="fade-in"  id="route" class="route">
    <div class="route-header">
        <h2 id="important"><?= $item3 ?></h2>
    </div>
    <div class="route-content">
        <ul>
            <li><span class="year">2018</span><span class="desc"><?= $route1 ?></span></li>
            <li><span class="year">2018</span><span class="desc"><?= $route2 ?></span></li>
            <li><span class="year">2019</span><span class="desc"><?= $route3 ?></span></li>
            <li><span class="year">2021</span><span class="desc"><?= $route4 ?></span></li>
            <li><span class="year">2021</span><span class="desc"><?= $route5 ?></span></li>
        </ul>
    </div>
</div>