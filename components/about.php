<div data-aos="fade-in" id="about" class="about">
    <div class="about-text">
        <div class="about-text-content">
            <h2 id="important"><?= $item1 ?></h2>
            <p><?= $aboutme ?></p>    
        </div>
    </div>
    <div class="about-image">
        <img src="ressource/img/bg.jpg" alt="">
    </div>
</div>