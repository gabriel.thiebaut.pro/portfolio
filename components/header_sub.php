<!-- HEADER SUB IN PHP -->

<div data-aos="fade-in" class="header-sub">
    <div class="header-sub-text">
        <div class="header-sub-text-title">
            <h1><?php echo $firstname . ' ' . $lastname ?></h1>
            <h2><?php echo $subtitle ?></h2>
        </div>
        <div class="header-sub-text-button">
            <a href="#contact"><span>CONTACT</span></a>
        </div>
    </div>
    <div class="wrapper">
        <a href="https://instagram.com/hayross.gab/">
            <div class="icon instagram">
                <div class="tooltip">Instagram</div>
                <span><i class="fab fa-instagram"></i></span>
            </div>
        </a>
        <a href="https://gitlab.com/gabriel.thiebaut.pro">
            <div class="icon github">
                <div class="tooltip">GitLab</div>
                <span><i class="fab fa-gitlab"></i></span>
            </div>
        </a>
    </div>
</div>