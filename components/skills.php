<div data-aos="fade-in"  id="skills" class="skills">
    <div class="skills-header">
        <h2 id="important"><?= $item2 ?></h2>
    </div>
    <div class="skills-cards">
        <div class="card-skill">
            <div class="card-skill-header">
                <h2>HTML</h2>
            </div>
            <div class="card-skill-content">
                <span class="evidence"><strong><?= $level ?> : </strong> <?= $advanced ?></span>
            </div>
        </div>
        <div class="card-skill">
            <div class="card-skill-header">
                <h2>CSS</h2>
            </div>
            <div class="card-skill-content">
                <span class="evidence"><strong><?= $level ?> : </strong> <?= $advanced ?></span>
            </div>
        </div>
        <div class="card-skill">
            <div class="card-skill-header">
                <h2>JS</h2>
            </div>
            <div class="card-skill-content">
                <span class="evidence"><strong><?= $level ?> : </strong> <?= $low ?></span>
            </div>
        </div>
        <div class="card-skill">
            <div class="card-skill-header">
                <h2>PHP</h2>
            </div>
            <div class="card-skill-content">
                <span class="evidence"><strong><?= $level ?> : </strong> <?= $low ?></span>
            </div>
        </div>
    </div>
</div>