<div data-aos="fade-in" id="projects" class="projects">
    <div class="projects-header">
        <h2 id="important"><?= $item4 ?></h2>
    </div>
    <div class="projects-content">
        <div style="background-image:url('ressource/img/htmlimg.jpg')" class="project-card">
            <a href="https://gitlab.com/gabriel.thiebaut.pro/brief-html-css">
                <div class="project-card-content">
                    <h2><?= $htmlcss ?></h2>
                </div>
            </a>
        </div>
        <div style="background-image:url('ressource/img/cssimg.jpg')" class="project-card">
            <a href="https://gitlab.com/gabriel.thiebaut.pro/brief-integration-analyse">
                <div class="project-card-content">
                    <h2><?= $analyse ?></h2>
                </div>
            </a>
        </div>
        <div style="background-image:url('ressource/img/jsimg.jpg')" class="project-card">
            <a href="">
                <div class="project-card-content">
                    <h2><?= $animation ?></h2>
                </div>
            </a>
        </div>
        <div style="background-image:url('ressource/img/phpimg.jpg')" class="project-card">
            <a href="https://gitlab.com/gabriel.thiebaut.pro/brief-php-mysql">
                <div class="project-card-content">
                    <h2><?= $bookmarks ?></h2>
                </div>
            </a>
        </div>
        <div style="background-image:url('ressource/img/htmlimg.jpg')" class="project-card">
            <a href="https://gitlab.com/gabriel.thiebaut.pro/portfolio">
                <div class="project-card-content">
                    <h2><?= $portfolio ?></h2>
                </div>
            </a>
        </div>
    </div>
</div>