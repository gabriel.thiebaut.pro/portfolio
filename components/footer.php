<footer>
    <div class='footer-socials'>
        <div class="wrapper">
            <a href="https://instagram.com/hayross.gab/">
                <div class="icon instagram">
                    <div class="tooltip">Instagram</div>
                    <span><i class="fab fa-instagram"></i></span>
                </div>
            </a>
            <a href="https://gitlab.com/gabriel.thiebaut.pro">
                <div class="icon github">
                    <div class="tooltip">GitLab</div>
                    <span><i class="fab fa-gitlab"></i></span>
                </div>
            </a>
        </div>
    </div>
    <div class='footer-copy'>
        <p>Developed by Gabriel Thiebaut</p>
    </div>
    <div class='footer-other'>

    </div>
</footer>