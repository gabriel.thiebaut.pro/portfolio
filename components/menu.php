<div id="menu" class="menu-inactive">
    <nav>
        <ul>
            <li><a href="#about"><?= $item1 ?></a></li>
            <li><a href="#skills"><?= $item2 ?></a></li>
            <li><a href="#route"><?= $item3 ?></a></li>
            <li><a href="#projects"><?= $item4 ?></a></li>
            <li><a href="#contact"><?= $item5 ?></a></li>
            <form class="langselect" method="get">
                <select name="lang" id="lang">
                    <option value="fr">-- Language --</option>
                    <option value="fr">Français</option>
                    <option value="en">English</option>
                </select>
                <input type="submit">
            </form>
        </ul>
    </nav>
</div>