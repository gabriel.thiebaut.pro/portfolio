<!-- HEADER FILE IN PHP -->

<header data-aos="fade-in" >
    <div class="hd-menu">
        <nav>
            <ul>
                <a href="#about">
                    <li><?php echo $item1 ?></li>
                </a>
                <a href="#skills">
                    <li><?php echo $item2 ?></li>
                </a>
                <a href="#route">
                    <li><?php echo $item3 ?></li>
                </a>
                <a href="#projects">
                    <li><?php echo $item4 ?></li>
                </a>
                <a href="#contact">
                    <li><?php echo $item5 ?></li>
                </a>
            </ul>
        </nav>
        <form class="langselect" method="get">
            <select name="lang" id="lang">
                <option value="fr">-- Language --</option>
                <option value="fr">Français</option>
                <option value="en">English</option>
            </select>
            <input type="submit">
        </form>
    </div>
</header>